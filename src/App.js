import './App.css';
import Home from "./views/Home";
import PageNotFound from './views/PageNotFound';
import { Fragment, useEffect } from 'react';
import { BrowserRouter, Switch, Route} from "react-router-dom";
import { useDispatch } from 'react-redux';
import {fetchMe} from './store/actions/auth';

function App() {
  const dispatch = useDispatch();
  const token = localStorage.getItem("token");

  useEffect(() => {
    if(token) (dispatch(fetchMe))
 }, [dispatch,token]);

  return (
    <Fragment>
    <BrowserRouter>
        <Switch>
          <Route path="/" exact component={Home} />
          <Route path="*" redirectPath="/" component={PageNotFound} />
        </Switch>
    </BrowserRouter>
    </Fragment>
  );
}

export default App;
