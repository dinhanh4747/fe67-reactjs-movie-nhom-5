import React from 'react';
import Layout from '../../HOCs/Layout';
import Application from '../../components/Application';
import CarouselBanners from '../../components/CarouselBanners';
import Cinemas from '../../components/Cinemas';

function Home(props) {
    return (
        <Layout>
            Home
            <CarouselBanners/>
            <Cinemas/>
            <Application />
        </Layout>
    );
}

export default Home;