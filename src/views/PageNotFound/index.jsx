import React from 'react';
import Layout from '../../HOCs/Layout';

function PageNotFount(props) {
    return (
        <Layout>
                Page not found
        </Layout>
    );
}

export default PageNotFount;