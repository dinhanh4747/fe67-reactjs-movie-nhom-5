import moment from "moment";
import React from "react";

function CinemasSchedule(props) {
  const { tenPhim, hinhAnh, hot, dangChieu, lstLichChieuTheoPhim } = props.item;
  return (
    <div className="mb-2 border-b border-gray-100 md:px-6 pl-3 pr-6">
      <div className="flex cinema-desc">
        <img className="w-8 h-12 mr-2" src={hinhAnh} alt={tenPhim} />
        <div>
          <div>
            <h3 className="mr-2 capitalize font-medium">{tenPhim}</h3>
          </div>
          <div className="flex mb-2 text-white text-xs">
            {hot ? (
              <span className="bg-red-500 mr-2 p-1 rounded font-bold">HOT</span>
            ) : (
              <div></div>
            )}
            {dangChieu ? (
              <div className="bg-green-500 mr-2 p-1 rounded">In Theaters </div>
            ) : (
              <div className="bg-gray-500 mr-2 p-1 rounded">Coming Soon</div>
            )}
          </div>
        </div>
      </div>
      <div className="grid 2xl:grid-cols-6 xl:grid-cols-5 lg:grid-cols-4 sm:grid-cols-3 grid-cols-4 gap-4 mb-2">
        {lstLichChieuTheoPhim.slice(0, 12).map((item) => {
          return (
            <div>
              {/* <div className="text-center text-sm text-gray-700">{item.tenRap}</div> */}
              <div
                className="border border-gray-100 p-1 text-sm text-gray-700 text-center rounded"
                key={item.maLichChieu}
              >
                <span>{moment(item.ngayChieuGioChieu).format("hh:mm A")}</span>
              </div>
            </div>
          );
        })}
      </div>
    </div>
  );
}

export default CinemasSchedule;
