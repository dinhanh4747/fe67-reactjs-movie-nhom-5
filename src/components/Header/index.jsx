import React, { useState } from 'react';
import Logo from '../../assets/logo.png';
import SignUp from '../../views/SignUp';
import SignIn from '../../views/SignIn';
import { useDispatch, useSelector } from 'react-redux';
import { createAction } from "../../store/actions";
import { actionType } from "../../store/actions/type";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faUserCircle} from "@fortawesome/free-solid-svg-icons";
import Notification from '../../views/Notification';

function Header(props) {
  const [isSignIn, setSignIn] = useState(false);
  const [isSignUp, setSignUp] = useState(false);
  const [isNotification, setNotification] = useState("false");
  
  const dispatch = useDispatch();

  const {taiKhoan} = useSelector((state) =>{
    return state.user || [];
  })
  const handleSignOut = () =>{
    localStorage.clear();
    dispatch(createAction(actionType.DEL_USER,[]));
  }

  return (
    <div>
      Header
     <img src={Logo} style={{width:"100px"}} alt="Logo Movie Time" />
     
     
     {/* ------------------------------------------------------------- */}
     {!taiKhoan?
       <>
       <button onClick={() => setSignIn(true)}>Sign In</button>
       <button onClick={() => setSignUp(true)}>Sign Up</button>
      </>:
      <>
       <FontAwesomeIcon icon={faUserCircle} />
       <button >Hello! {taiKhoan}</button>
       <button onClick={handleSignOut}>Sign Out</button>
      </>
     }
    
     <SignUp isSignUp={isSignUp} setSignUp={setSignUp} setSignIn={setSignIn} setNotification={setNotification}/>
     <SignIn isSignIn={isSignIn} setSignIn={setSignIn} setSignUp={setSignUp} setNotification={setNotification}/>
     <Notification isNotification={isNotification} setNotification={setNotification}  />
    </div>
  );
}

export default Header;