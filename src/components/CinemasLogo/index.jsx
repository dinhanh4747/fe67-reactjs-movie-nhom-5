import React from 'react';
import { useDispatch } from 'react-redux';
import { fetchCinemaByAddress } from '../../store/actions/cinemas';
import "./styles.css";

function CinemasLogo(props) {
    const {maHeThongRap,logo,tenHeThongRap} = props.item;
    const dispatch = useDispatch();

    const handleCinema = (cinemaID) => {
        dispatch(fetchCinemaByAddress(cinemaID));
        console.log(cinemaID);
    };
    return (
        <div key={maHeThongRap} className="w-1/12 m-3 ">
            <img onClick={() => handleCinema(maHeThongRap)} className="opacity-80 cursor-pointer shadow w-full rounded-full cinema-logo" src={logo} alt={tenHeThongRap} />
        </div>
    );
}

export default CinemasLogo;