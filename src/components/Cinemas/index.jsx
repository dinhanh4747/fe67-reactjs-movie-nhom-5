import React, { useEffect } from "react";
import "./styles.css";
import { fetchCinemas, fetchCinemaByAddress, fetchCinemaSchedule } from "../../store/actions/cinemas";
import { useDispatch, useSelector } from "react-redux";
import CinemasLogo from "../CinemasLogo";
import CinemasItem from "../CinemasItem";
import CinemasSchedule from "../CinemasSchedule";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {faCalendarAlt} from "@fortawesome/free-solid-svg-icons";

function Cinemas(props) {
  const dispatch = useDispatch();

  const cinemaList = useSelector((state) => {
    return state.cinemas.cinemaList || [];
  });

  const cinemaByAddress = useSelector((state) => {
    return state.cinemas.cinemaByAddress || [];
  });

  const cinemaSchedule = useSelector((state) => {
    return state.cinemas.cinemaSchedule || [];
  });

  useEffect(() => {
    dispatch(fetchCinemas());
    dispatch(fetchCinemaByAddress("BHDStar"));
    dispatch(fetchCinemaSchedule('BHDStar','bhd-star-cineplex-3-2'));
  }, [dispatch]);

  return (
    <div className="bg-white cinema">
      <div className="container pb-8 border border-gray-100">
        <h2 className="text-center font-medium uppercase text-4xl p-4">
        <FontAwesomeIcon style={{  marginRight: "10px" }} icon={faCalendarAlt} />movie showtimes
        </h2>
        <div className="shadow ">
          <div className="flex p-3 cinema-logo border-b border-gray-100 ">
            {cinemaList.map((item) => {
              return <CinemasLogo key={item.maHeThongRap} item={item} />
            })}
          </div>
          <div className="sm:flex  grid cinema-content">
            <div className="sm:w-5/12 w-full cinema-list border-t border-gray-100">
              {
                cinemaByAddress.map((item, index) =>{
                 return <CinemasItem index={index} item={item} key={item.maCumRap} />
                })
              }
            </div>
            <div className="sm:w-7/12 w-full py-3 border-l border-t border-gray-100 cinema-schedule">
              {
                cinemaSchedule.map((item, index) => {
                  return <CinemasSchedule key={index} item={item}/>
                })
              }
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

export default Cinemas;
