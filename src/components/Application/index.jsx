import React from "react";
import "./styles.css";
import Mobile from "../../assets/mobile.png";
import CarouselMobile from "../CarouselMobile";
import {faDownload} from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

function Application(props) {
  return (
    <div className="bg-app mb-6">
      <div className="grid md:grid-cols-2 grid-cols-1 md:py-24 py-12 container gap-12 items-center">
        <div>
          <h3 className="text-justify text-white font-medium text-4xl">
            Ứng dụng tiện lợi dành cho người yêu điện ảnh
          </h3>
          <p className="text-justify text-white py-6">
            Không chỉ đặt vé, bạn còn có thể bình luận phim, chấm điểm rạp và
            đổi quà hấp dẫn.
          </p>
          <button
            className="p-2 text-white font-medium text-xl"
            style={{ backgroundColor: "#ff6600" }}
          >
            App miễn phí <FontAwesomeIcon className="mx-1" icon={faDownload}/> Tải về ngay
          </button>
        </div>
        <div className="mx-auto caoursel-mobile">
          <CarouselMobile />
          <img className=" mx-auto caoursel-frame" src={Mobile} alt="" />
          
        </div>
      </div>
    </div>
  );
}

export default Application;
