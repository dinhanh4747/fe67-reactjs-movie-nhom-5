import React from "react";
import images from "../../assets";
import "./styles.css";
import {fetchCinemaSchedule} from '../../store/actions/cinemas';
import { useDispatch } from "react-redux";

function CinemasItem(props) {
  const { maCumRap, tenCumRap, diaChi } = props.item;
  const { BHD, CGV, Cinestar, Glaxy, Lotte, Mega } = images;
  
  const dispatch = useDispatch();

  const handleCinemaAddressID = (cinemaAddressID) => {
    let cinemaID="";
    switch (cinemaAddressID.slice(0, 3)) {
      case "cgv":
        cinemaID = "CGV"
        return dispatch(fetchCinemaSchedule(cinemaID, cinemaAddressID)) ;
      case "cns":
        cinemaID = "CineStar"
        return dispatch(fetchCinemaSchedule(cinemaID, cinemaAddressID)) ;  
      case "glx": 
        cinemaID = "Galaxy"
        return dispatch(fetchCinemaSchedule(cinemaID, cinemaAddressID)) ;
      case "lot":
        cinemaID = "LotteCinima"
        return dispatch(fetchCinemaSchedule(cinemaID, cinemaAddressID)) ;  
      case "meg":
        cinemaID = "MegaGS"
        return dispatch(fetchCinemaSchedule(cinemaID, cinemaAddressID)) ;  
      default:
        cinemaID = "BHDStar"
        return dispatch(fetchCinemaSchedule(cinemaID, cinemaAddressID)) ;  
    }
  }
  const renderCinemaByAddress = () => {
    switch (maCumRap.slice(0, 3)) {
      case "cgv":
        return (
          <>
            <img src={CGV} alt={tenCumRap} className="border border-gray-200 p-1 mr-2 w-8"/>
            <div className="w-full">
              <p className="font-medium cinema-name" style={{color:"#ee2d24"}}>{tenCumRap}</p>
              <p className="text-gray-600 text-sm cinema-address">{diaChi}</p>
            </div>
          </>
        );
      case "cns":
        return (
            <>
              <img src={Cinestar} alt={tenCumRap} className="border border-gray-200 p-1 mr-2 w-8"/>
              <div className="w-full">
                <p className="font-medium cinema-name" style={{color:"#662D91"}} >{tenCumRap}</p>
                <p className="text-gray-600 text-sm cinema-address">{diaChi}</p>
              </div>
            </>
          );
      case "glx":
        return (
            <>
              <img src={Glaxy} alt={tenCumRap} className="border border-gray-200 p-1 mr-2 w-8"/>
              <div className="w-full">
                <p className="font-medium cinema-name" style={{color:"#FF8700"}}>{tenCumRap}</p>
                <p className="text-gray-600 text-sm cinema-address">{diaChi}</p>
              </div>
            </>
          );
      case "lot":
        return (
            <>
              <img src={Lotte} alt={tenCumRap} className="border border-gray-200 p-1 mr-2 w-8"/>
              <div className="w-full">
                <p className="font-medium cinema-name" style={{color:"#ED1B24"}}>{tenCumRap}</p>
                <p className="text-gray-600 text-sm cinema-address">{diaChi}</p>
              </div>
            </>
          );
      case "meg":
        return(
            <>
              <img src={Mega} alt={tenCumRap} className="border border-gray-200 p-1 mr-2 w-8"/>
              <div className="w-full">
                <p className="font-medium cinema-name" style={{color:"#E5A912"}}>{tenCumRap}</p>
                <p className="text-gray-600 text-sm cinema-address">{diaChi}</p>
              </div>
            </>
          );
      default:
        return (
            <>
              <img src={BHD} alt={tenCumRap} className="border border-gray-200 p-1 mr-2 w-8"/>
              <div className="w-full">
                <p className="font-medium cinema-name" style={{color:"#8BC543"}}>{tenCumRap}</p>
                <p className="text-gray-600 text-sm cinema-address">{diaChi}</p>
              </div>
            </>
          );
    }
  };
 
  return (
    <div onClick={() => handleCinemaAddressID(maCumRap)} className="md:px-6 pl-3 pr-6 md:py-3 py-2 flex items-center border-b border-gray-100 cursor-pointer hover:bg-gray-50 cinema-item">
     {
        renderCinemaByAddress()
     }
    </div>
  );
}

export default CinemasItem;
