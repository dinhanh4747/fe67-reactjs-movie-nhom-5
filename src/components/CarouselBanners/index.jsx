import React, {useEffect, useRef } from "react";
import Slider from "react-slick";
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";
import { fetchBanners } from "../../store/actions/carousels";
import { useDispatch } from "react-redux";
import { useSelector } from "react-redux";
import "./styles.css";

function CarouselBanners(props) {
  const dispatch = useDispatch();
  useEffect(() => {
    dispatch(fetchBanners()) ;
  }, [dispatch]);
  
  const carousels = useSelector((state) => {
    return state.carousel || [];
});


  const ref = useRef({});
  const settings = {
    className: "section-outstanding__slider",
    slidesToShow: 1,
    slidesToScroll: 1,
    infinite: true,
    autoplay: true,
    autoplaySpeed: 2000,
    dots:true,
  };
  
  return (
    <div className="movie-carousel w-full">
    <Slider ref={ref} {...settings}>
      {carousels.map((item, index) => {
        return (
         <div key={index} className="movie-carousel-item"> 
            <img className="w-full" src={item.hinhAnh} alt="" /> 
          </div>
        );
      })}
    </Slider>
    </div>
  );
}

export default CarouselBanners;
