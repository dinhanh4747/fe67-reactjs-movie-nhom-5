import React from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {faFacebookF,faTwitter,faInstagram,} from "@fortawesome/free-brands-svg-icons";
import {faMapMarkerAlt,faPhoneSquare,faEnvelope,} from "@fortawesome/free-solid-svg-icons";
import images from '../../assets';
import "./styles.css";

function Footer(props) {
  const { Logo,BT,BHD,CGV, Cinestar,CNX,Dcine,Glaxy,Lotte,Mega,Startlight,Touch,Apple,Google} = images;
  return (
    <div className="movie-footer">
      <div className="container mb-4">
        <div className="grid md:grid-cols-12 sm:grid-cols-2 grid-cols-1 gap-x-4">
          <div className="md:col-span-3">
            <h2 className="py-2 font-semibold uppercase tracking-widest leading-10 ">
              about us
            </h2>
            <a href=".">
              <img
                className="mx-auto"
                src={Logo}
                style={{ width: "100px" }}
                alt="Logo Movie Time"
              />
            </a>
            <p className="text-justify pt-1 text-secondary">
              Movie Time - Ứng dụng đặt vé xem phim Online hàng đầu.
            </p>
          </div>
          <div className="md:col-span-3">
            <h2 className="py-2 font-semibold uppercase tracking-widest leading-10 ">
              partners
            </h2>
            <div className="grid grid-cols-3 gap-y-4">
              <a href=".">
                <img className="w-3/6 bg-white rounded-full" src={BHD} alt="" />
              </a>
              <a href=".">
                <img className="w-3/6 bg-white rounded-full" src={BT} alt="" />
              </a>
              <a href=".">
                <img className="w-3/6 bg-white rounded-full" src={CGV} alt="" />
              </a>
              <a href=".">
                <img
                  className="w-3/6 bg-white rounded-full"
                  src={Cinestar}
                  alt=""
                />
              </a>
              <a href=".">
                <img className="w-3/6 bg-white rounded-full" src={CNX} alt="" />
              </a>
              <a href=".">
                <img
                  className="w-3/6 bg-white rounded-full"
                  src={Dcine}
                  alt=""
                />
              </a>
              <a href=".">
                <img
                  className="w-3/6 bg-white rounded-full"
                  src={Touch}
                  alt=""
                />
              </a>
              <a href=".">
                <img
                  className="w-3/6 bg-white rounded-full"
                  src={Glaxy}
                  alt=""
                />
              </a>
              <a href=".">
                <img
                  className="w-3/6 bg-white rounded-full"
                  src={Lotte}
                  alt=""
                />
              </a>
              <a href=".">
                <img
                  className="w-3/6 bg-white rounded-full"
                  src={Startlight}
                  alt=""
                />
              </a>
              <a href=".">
                <img
                  className="w-3/6 bg-white rounded-full"
                  src={Mega}
                  alt=""
                />
              </a>
            </div>
          </div>
          <div className="md:col-span-2">
            <h2 className="py-2 font-semibold uppercase tracking-widest leading-10">
              social
            </h2>
            <ul className="leading-relaxed list-none text-secondary">
              <li className="capitalize">
                <a href=".">
                  <FontAwesomeIcon
                    style={{ width: "20px", marginRight: "5px" }}
                    icon={faFacebookF}
                  />facebook
                </a>
              </li>
              <li className="capitalize">
                <a href=".">
                  <FontAwesomeIcon style={{ width: "20px", marginRight: "5px" }} icon={faTwitter} />twitter
                </a>
              </li>
              <li className="capitalize">
                <a href=".">
                  <FontAwesomeIcon
                   style={{ width: "20px", marginRight: "5px" }}
                    icon={faInstagram}
                  />instagram
                </a>
              </li>
            </ul>
          </div>
          <div className="md:col-span-4">
            <h2 className=" py-2 font-semibold uppercase tracking-widest leading-10 ">
              contact
            </h2>
            <ul className="text-justify leading-relaxed list-none text-secondary">
              <li className="capitalize ">
                <a href="." >
                  <FontAwesomeIcon
                    style={{ width: "20px", marginRight: "5px" }}
                    icon={faMapMarkerAlt}
                  />208 Nguyễn Hữu Cảnh, Bình Thạnh, Hồ Chí Minh
                </a>
              </li>
              <li className="capitalize">
                <a href="tel:+84987654321">
                  <FontAwesomeIcon
                    style={{ width: "20px", marginRight: "5px" }}
                    icon={faPhoneSquare}
                  />098.765.4321
                </a>
              </li>
              <li>
                <a href="mailto:movietime@gmail.com">
                  <FontAwesomeIcon
                    style={{ width: "20px", marginRight: "5px" }}
                    icon={faEnvelope}
                  />movietime@gmail.com
                </a>
              </li>
            </ul>
          </div>
        </div>
      </div>
      <hr className="my-6" />
      <div className="container mb-4">
        <div className="grid md:grid-cols-3 grid-cols-1 text-secondary">
          <p className="md:text-left text-center py-2 my-auto">
            <a href="." >Terms And Policies</a>
          </p>
          <p className="md:block hidden  py-2 my-auto">2021 © Movie Time</p>
          <div className="py-2 flex justify-between">
            <a href="." >
              <img className="w-4/5" src={Apple} alt="Apple Store" />
            </a>
            <a href="." >
              <img className="ml-auto w-4/5" src={Google} alt="Google Store" />
            </a>
          </div>
        </div>
      </div>
    </div>
  );
}

export default Footer;
