import { createAction } from ".";
import { request } from "../../api/request";
import { actionType } from "./type";

export const fetchCinemas = () => {
  return (dispatch) => {
    request({
      method: "GET",
      url: "http://movienew.cybersoft.edu.vn/api/QuanLyRap/LayThongTinHeThongRap",
      data: "",
    })
      .then((res) => {  
        dispatch(createAction(actionType.SET_CINEMA,res.data.content));
      })
      .catch((err) => {
        console.log(err);
      });
  };
};

export const fetchCinemaByAddress = (maHeThongRap) =>{
  return(dispatch) => {
    request({
      method:"GET",
      url: "http://movienew.cybersoft.edu.vn/api/QuanLyRap/LayThongTinCumRapTheoHeThong",
      params:{
        maHeThongRap:maHeThongRap
      }
    }).then((res) => {      
      dispatch(createAction(actionType.SET_CINEMA_BY_ADDRESS, res.data.content));
    }).catch((err) => {
      console.log(err.response);
    });
  };
};

export const fetchCinemaSchedule = (maHeThongRap, maCumRap) => {
  return(dispatch) =>{
    request({
      method:"GET",
      url:"http://movienew.cybersoft.edu.vn/api/QuanLyRap/LayThongTinLichChieuHeThongRap",
      params:{
        maHeThongRap:maHeThongRap,
        maNhom:"GP01",
      }
    }).then((res) => {
      let listPhim = res.data.content[0].lstCumRap.find(cinemas=> cinemas.maCumRap===maCumRap);
      console.log(listPhim);
      dispatch(createAction(actionType.SET_CINEMA_SCHEDULE, listPhim.danhSachPhim));
  
    }).catch((err) => {
       console.log(err);
    });
  } 
}