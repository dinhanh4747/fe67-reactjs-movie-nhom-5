import { request } from "../../api/request";
import { createAction } from "./index";
import { actionType } from "./type";

export const signIn = (userLogin, handleCloseSignIn, openNotification) => {
    return (dispatch) => {
        request({
            url: "https://movienew.cybersoft.edu.vn/api/QuanLyNguoiDung/DangNhap",
            method:"POST",
            data:userLogin,
        })
        .then((res) => {
            dispatch(createAction(actionType.SET_USER,{...res.data.content, accessToken:""}));
            localStorage.setItem("token",res.data.content.accessToken);
            openNotification("Chúc mừng bạn đã đăng nhập thành công.");
            handleCloseSignIn();
            
        })
        .catch((err) => {
            openNotification(err.response.data.content);
        });
    };
};

export const signUp = (userSignup, callback,openNotification) => {
 request({
    url:"https://movienew.cybersoft.edu.vn/api/QuanLyNguoiDung/DangKy",
    method:"POST",
    data:userSignup,
 }).then((res) => {
    openNotification("Chúc mừng bạn đã đăng kí thành công tài khoản "+userSignup.taiKhoan +".");
    callback();
 }).catch((err) => {
    openNotification(err.response.data.content);
 })
};
export const fetchMe = (dispatch) => {
    request({
        method: "POST",
        url: "https://movienew.cybersoft.edu.vn/api/QuanLyNguoiDung/ThongTinTaiKhoan",
      }).then((res)=>{
        dispatch(createAction(actionType.SET_USER, res.data.content));
      }).catch((err)=>{
        console.log("loi ne");
        console.log(err.response);
      });
  };
  