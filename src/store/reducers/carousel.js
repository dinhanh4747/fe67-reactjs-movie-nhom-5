import { actionType } from "../actions/type";

const initialState = [];

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case actionType.SET_BANNER:
      state = action.payload;
      return [ ...state ];
    default:
      return state;
  }
};

export default reducer;
