import { actionType } from "../actions/type";

const initialState = {
  cinemaList: null,
  cinemaByAddress: null,
  cinemaSchedule:null,
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case actionType.SET_CINEMA:
      state.cinemaList = action.payload;
      return { ...state };
    case actionType.SET_CINEMA_BY_ADDRESS:
      state.cinemaByAddress = action.payload;
      return { ...state };
    case actionType.SET_CINEMA_SCHEDULE:
      state.cinemaSchedule = action.payload;
      return { ...state };
    default:
      return state;
  }
};

export default reducer;
