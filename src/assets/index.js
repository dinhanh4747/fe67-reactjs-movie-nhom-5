import Logo from "../assets/logo.png";
import BHD from "../assets/cinema/bhd.png";
import BT from "../assets/cinema/bt.jpg";
import CGV from "../assets/cinema/cgv.png";
import Cinestar from "../assets/cinema/cinestar.png";
import CNX from "../assets/cinema/cnx.jpg";
import Dcine from "../assets/cinema/dcine.png";
import Glaxy from "../assets/cinema/galaxycine.png";
import Lotte from "../assets/cinema/lotte.png";
import Mega from "../assets/cinema/megags.png";
import Startlight from "../assets/cinema/STARLIGHT.png";
import Touch from "../assets/cinema/TOUCH.png";
import Apple from "../assets/app-store.png";
import Google from "../assets/google-play.png";
const images = {
    Logo,
    BT,
    BHD,
    CGV,
    Cinestar,
    CNX,
    Dcine,
    Glaxy,
    Lotte,
    Mega,
    Startlight,
    Touch,
    Apple,
    Google,
}
export default images;